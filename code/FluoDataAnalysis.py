from urllib.request import urlretrieve
urlretrieve("https://zenodo.org/records/10808045/files/Data_POMC.hdf5?download=1",
            "../data/Data_POMC.hdf5")

urlretrieve("https://zenodo.org/records/10808045/files/CCD_calibration.hdf5?download=1",
            "../data/CCD_calibration.hdf5")

import h5py

pomc = h5py.File("../data/Data_POMC.hdf5","r")
list(pomc)

print(pomc.attrs['README'])

time_pomc = pomc['time'][...]
stack_pomc = pomc['stack'][...]

pomc.close()

def plotSignal(stack,lw=1,color='black',offset=0):
    import numpy as np
    import matplotlib.pyplot as plt
    n_x, n_y, n_t = stack.shape
    amp_min = np.min(stack)
    amp_max = np.max(stack)
    amp_diff = np.ptp(stack)
    x_domain = np.arange(n_t)/n_t
    y_domain = (0,n_y)
    for r_idx in range(n_x):
        for c_idx in range(n_y):
            y_min = n_x - r_idx - 1 - offset
            sig = stack[r_idx,c_idx,:]
            Y = (sig-amp_min)/amp_diff + y_min
            X = x_domain + c_idx - offset
            plt.plot(X,Y,lw=lw,color=color)
            plt.ylim([0,n_y-1])
    plt.axis('off')

import numpy as np
import matplotlib.pylab as plt

from matplotlib.patches import Rectangle
stack_pomc_m = np.mean(stack_pomc[:,:,:15],axis=2)
plt.figure(dpi=400,figsize=(15,10))
plt.subplot(1,2,1)
plt.imshow(stack_pomc_m[:,23:61],cmap="gray")
ax = plt.gca()
rect = Rectangle((10-0.5,20+0.5),11,13,linewidth=2,edgecolor='orange',facecolor='none')
ax.add_patch(rect)
plt.axis('off')
plt.subplot(1,2,2)
plotSignal(stack_pomc[20:33,33:44,:],lw=0.5)
fname = '../figs/pomc_1.png'
plt.savefig(fname)
plt.close()
fname

plt.figure(dpi=600,figsize=(10,8))
plt.plot(time_pomc,stack_pomc[27,39,:],lw=2)
plt.xlabel("Time (s)",fontsize=25)
plt.ylabel("ADU count",fontsize=25)
plt.grid()
plt.xlim([525,550])
fname = '../figs/pomc_2.png'
plt.savefig(fname)
plt.close()
fname

calibration = h5py.File("../data/CCD_calibration.hdf5","r")
list(calibration)

print(calibration.attrs['README'])

plt.figure(dpi=600,figsize=(10,8))
plt.imshow(np.transpose(calibration['10ms/stack'][:,:,0]),origin='lower')
plt.set_cmap('gray')
plt.colorbar()
fname = "../figs/exposition1.png"
plt.savefig(fname)
plt.close()
fname

plt.figure(dpi=600,figsize=(10,8))
plt.subplot(311)
plt.plot(calibration['10ms/stack'][31,41,:])
plt.ylabel("ADU",fontsize=25)
plt.grid()
plt.subplot(312)
plt.plot(calibration['10ms/stack'][31,40,:])
plt.ylabel("ADU",fontsize=25)
plt.grid()
plt.subplot(313)
plt.plot(calibration['10ms/stack'][31,39,:])
plt.xlabel("Time (1 unit = 100 ms)",fontsize=25)
plt.ylabel("ADU",fontsize=25)
plt.grid()
fname = "../figs/evolution1.png"
plt.savefig(fname)
plt.close()
fname

D_matrix = np.transpose(np.array([np.ones(100),np.arange(100)]))
P_matrix = np.linalg.solve(np.dot(np.transpose(D_matrix),D_matrix),np.transpose(D_matrix))
Y = calibration['10ms/stack'][31,40,:]
beta = np.dot(P_matrix,Y)
beta[1]

Y_hat = np.dot(D_matrix,beta)
s2_hat = np.sum((Y-Y_hat)**2)/98
beta_se = np.sqrt(s2_hat*np.linalg.inv(np.dot(np.transpose(D_matrix),D_matrix)))
beta_se[1,1]

from scipy.stats import t
(beta[1]-beta_se[1,1]*t.ppf(0.975,98),beta[1]+beta_se[1,1]*t.ppf(0.975,98))

plt.figure(dpi=600,figsize=(10,8))
plt.plot(D_matrix[:,1],Y,lw=2,color='black')
plt.grid()
plt.xlabel("Time (1 unit = 100 ms)",fontsize=25)
plt.ylabel("ADU",fontsize=25)
plt.plot(D_matrix[:,1],Y_hat,lw=2,color='red')
fname = "../figs/evolution1_fit.png"
plt.savefig(fname)
plt.close()
fname

def linear_fit_stack(stack):
    I,J,K = stack.shape
    D_matrix = np.transpose(np.array([np.ones(K),np.arange(K)]))
    P_matrix = np.linalg.solve(np.dot(np.transpose(D_matrix),D_matrix),np.transpose(D_matrix))
    the_inv = np.linalg.inv(np.dot(np.transpose(D_matrix),D_matrix))[1,1]
    res = np.zeros((I,J))
    for i in range(I):
        for j in range(J):
            beta = np.dot(P_matrix,stack[i,j,:])
            Y_hat = np.dot(D_matrix,beta)
            s2_hat = np.sum((stack[i,j,:]-Y_hat)**2)/(K-2)
            res[i,j] = beta[1]/np.sqrt(s2_hat*the_inv)
    return res

b1stats = [linear_fit_stack(calibration[n+'/stack']) for n in list(calibration)]

[[n for n in list(calibration)[:5]], [int(1000*np.max(np.abs(np.arange(1,len(b1s)+1)/len(b1s)-t.cdf(b1s,98))))/1000 for b1s in [np.sort(b1.flatten()) for b1 in b1stats[:5]]]]

[[n for n in list(calibration)[5:]], [int(1000*np.max(np.abs(np.arange(1,len(b1s)+1)/len(b1s)-t.cdf(b1s,98))))/1000 for b1s in [np.sort(b1.flatten()) for b1 in b1stats[5:]]]]

tt = np.linspace(-4,4,501)
plt.figure(dpi=600,figsize=(10,8))
junk = plt.hist(np.concatenate([b1.flatten() for b1 in b1stats]),
                bins=100,density=True,color='black')
plt.xlabel(r'$\hat{\beta}_1/\hat{\sigma}_{\beta_1}$',fontsize=25)
plt.ylabel('Density',fontsize=25)
plt.title(r'Density of all $\hat{\beta}_1/\hat{\sigma}_{\beta_1}$',fontsize=25)
plt.plot(tt,t.pdf(tt,98),color='orange',lw=5)
fname = '../figs/hist_beta1_stats_all.png'
plt.savefig(fname)
plt.close()
fname

def correlations(stack):
    n_row, n_col, n_time = stack.shape
    n_pixel = n_row*n_col
    result = np.zeros((n_pixel*(n_pixel-1)//2,))
    stack_score = np.copy(stack)
    stack_score = (stack_score - stack_score.mean(2).\
                   reshape((n_row,n_col,1)))/stack_score.std(2).\
                   reshape((n_row,n_col,1))
    idx = 0
    for i in range(n_pixel-1):
        for j in range(i+1,n_pixel):
            pos1 = (i//n_col,i-(i//n_col)*n_col)
            pos2 = (j//n_col,j-(j//n_col)*n_col)
            coef = np.sum(stack_score[pos1[0],pos1[1],:]*\
                          stack_score[pos2[0],pos2[1],:])/n_time
            result[idx] = coef
            idx += 1 
    return result

corr10 = correlations(calibration['10ms/stack'])

from scipy.stats import norm
plt.figure(dpi=600,figsize=(10,8))
hist_corr10 = plt.hist(corr10,bins=100,density=True,color='black')
plt.xlabel(r'$\rho(ij,uv)$',fontsize=25)
plt.ylabel('Density',fontsize=25)
plt.title('Density of correlation coefficients at 10 ms',fontsize=25)
plt.plot(tt/10,norm.pdf(tt/10,0,0.1),color='orange',lw=5)
fname = '../figs/hist_corr10_plus_theoretical.png'
plt.savefig(fname)
plt.close()
fname

var_of_corr_list = [np.var(correlations(calibration[n+'/stack'])) for n in list(calibration)]

[[n for n in list(calibration)[:5]], [int(100000*v)/1000 for v in var_of_corr_list[:5]]]

plt.figure(dpi=600,figsize=(10,8))
for x,y in zip([np.mean(calibration[n+'/stack'],2) for n in list(calibration)],
               [np.var(calibration[n+'/stack'],2) for n in list(calibration)]):
    plt.scatter(x.flatten(),y.flatten(),0.05)

plt.xlabel(r'$\overline{ADU}$',fontsize=25)
plt.ylabel("Var(ADU)",fontsize=25)
plt.xlim([0,4500])
plt.ylim([0,1000])
fname = '../figs/var_vs_mean_calibration_1.png'
plt.savefig(fname)
plt.close()
fname

X_k = np.concatenate([x.flatten() for x in [np.mean(calibration[n+'/stack'],2) for n in list(calibration)]])
y_k = np.concatenate([x.flatten() for x in [np.var(calibration[n+'/stack'],2) for n in list(calibration)]])
sigma2_k = 2*y_k**2/(calibration['10ms/stack'].shape[2]-1)
Z = sum(1/sigma2_k)
num1 = sum(X_k/sigma2_k*(y_k-sum(y_k/sigma2_k)/Z))
denom1 = sum(X_k/sigma2_k*(X_k-sum(X_k/sigma2_k)/Z))
b_hat0 = num1/denom1
a_hat0 = sum((y_k-b_hat0*X_k)/sigma2_k)/Z

plt.figure(dpi=600,figsize=(10,8))
for x,y in zip([np.mean(calibration[n+'/stack'],2) for n in list(calibration)],
               [np.var(calibration[n+'/stack'],2) for n in list(calibration)]):
    plt.scatter(x.flatten(),y.flatten(),0.05)

plt.xlabel(r'$\overline{ADU}$',fontsize=25)
plt.ylabel("Var(ADU)",fontsize=25)
aa = np.linspace(0,4500)
plt.plot(np.linspace(0,4500,101),a_hat0+b_hat0*np.linspace(0,4500,101),color='red',lw=2)
plt.xlim([0,4500])
plt.ylim([0,1000])
fname = '../figs/var_vs_mean_plus_fit_calibration_1.png'
plt.savefig(fname)
plt.close()
fname

G_hat0 = b_hat0
S2_hat0 = a_hat0/b_hat0**2 
(G_hat0, S2_hat0)

plt.figure(dpi=600,figsize=(10,8))
for x,y in zip([np.mean(calibration[n+'/stack'],2) for n in list(calibration)],
               [np.var(calibration[n+'/stack'],2) for n in list(calibration)]):
    plt.scatter(a_hat0+b_hat0*x.flatten(),(y.flatten()-a_hat0-b_hat0*x.flatten())*np.sqrt(99/2)/(a_hat0+b_hat0*x.flatten()),0.05)

plt.xlabel('Fitted value',fontsize=25)
plt.ylabel("Normalized residuals (Normal scores)",fontsize=25)
plt.axhline(0,color='red',lw=2)
fname = '../figs/residuals_vs_fitted_values_calibration_fit.png'
plt.savefig(fname)
plt.close()
fname

beta_true = 1.0
S_0 = 100
Delta = 900
X = np.linspace(0,5*beta_true,51)
Theo = Delta*np.exp(-X*beta_true)+S_0
np.random.seed(20061001)
Observations = np.random.poisson(Theo)

plt.plot(X,Observations,'o')
plt.xlim([0,5])
plt.ylim([0,1100])
plt.xlabel("Time (s)",fontsize=20)
plt.ylabel("Observations",fontsize=20)
plt.plot(X,Theo,'r')
plt.plot(X[[4,21]],Observations[[4,21]],'sk')
plt.plot([X[4],X[4]],[0,Observations[4]],'--k')
plt.plot([0,X[4]],[Observations[4],Observations[4]],'--k')
plt.plot([X[21],X[21]],[0,Observations[21]],'--k')
plt.plot([0,X[21]],[Observations[21],Observations[21]],'--k')
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.text(0.1,630,r'$y_1$',fontsize=20)
plt.text(1.5,110,r'$y_2$',fontsize=20)
fname = "../figs/mono-exp-sim.png"
plt.savefig(fname)
plt.close()
fname

from scipy.optimize import leastsq

def mk_residual_tilde(y_obs,t_obs=X[[4,21]],f_inf=S_0,step=Delta):
    import numpy as np
    return lambda beta: y_obs - f_inf - step * np.exp(-beta*t_obs)

def mk_residual_hat(y_obs,t_obs=X[[4,21]],f_inf=S_0,step=Delta):
    import numpy as np
    y_obs_sqrt = np.sqrt(y_obs)
    return lambda beta: y_obs_sqrt - np.sqrt(f_inf + step * np.exp(-beta*t_obs))

n_rep = int(1e5)
beta_bar = np.zeros(n_rep)
beta_tilde = np.zeros((n_rep,2))
beta_hat = np.zeros((n_rep,2))
pre_factor = -1.0/(X[21]-X[4])
np.random.seed(20110928)
for rep_idx in range(n_rep):
    Y = np.random.poisson(Theo[[4,21]])
    beta_start = pre_factor * np.log((Y[1] - S_0)/(Y[0] - S_0))
    beta_bar[rep_idx] = beta_start
    residual_tilde = mk_residual_tilde(Y)
    res_tilde = leastsq(residual_tilde,beta_start)
    beta_tilde[rep_idx,:] = [res_tilde[0][0],res_tilde[1]]
    residual_hat = mk_residual_hat(Y)
    res_hat = leastsq(residual_hat,beta_start)
    beta_hat[rep_idx,:] = [res_hat[0][0],res_hat[1]]

def Ffct(beta): 
    return Delta * np.exp(-X[[4,21]]*beta) + S_0

def dFfct(beta):
    return -X[[4,21]]*Delta * np.exp(-X[[4,21]]*beta)

sd0 = np.sqrt((np.sum(dFfct(1.0)**2*Ffct(1.0))/np.sum(dFfct(1.0)**2)**2))
sd1 = np.sqrt(1.0/np.sum(dFfct(1.0)**2/Ffct(1.0)))

beta_vector = np.linspace(0.6,1.6,501)
plt.hist([beta_tilde[:,0],beta_hat[:,0]], bins=50,
         density=True, histtype='step', lw=2)
plt.xlabel(r'$\beta$',fontsize=15)
plt.ylabel('Density',fontsize=15)
plt.plot(beta_vector,np.exp(-0.5*(beta_vector-1)**2/sd0**2)/sd0/np.sqrt(2*np.pi),
         color='blue',lw=2)
plt.plot(beta_vector,np.exp(-0.5*(beta_vector-1)**2/sd1**2)/sd1/np.sqrt(2*np.pi),
         color='orange',lw=2)
plt.grid()
plt.xticks(fontsize=12)
plt.yticks(fontsize=12)
plt.xlim([0.7,1.3])
fname = "../figs/betas_chap.png"
plt.savefig(fname)
plt.close()
fname

plt.figure(dpi=600,figsize=(10,8))
for x,y in zip([np.mean(2*np.sqrt(calibration[n+'/stack']/G_hat0+S2_hat0),2) for n in list(calibration)],
               [np.var(2*np.sqrt(calibration[n+'/stack']/G_hat0+S2_hat0),2) for n in list(calibration)]):
    plt.scatter(x.flatten(),y.flatten(),0.05)

plt.xlabel(r'$\mathrm{E}(2 \sqrt{ADU/\hat{G}+\hat{\sigma}_R^2})$',fontsize=20)
plt.ylabel(r'$\mathrm{Var}(2 \sqrt{ADU/\hat{G}+\hat{\sigma}_R^2})$',fontsize=20)
plt.xlim([100,350])
plt.axhline(1,color='red',lw=2)
fname = '../figs/varVSmeanStable1.png'
plt.savefig(fname)
plt.close()
fname

G_hat0 = 0.14
S2_hat0 = 290
stack_pomc_stab = 2*np.sqrt(np.copy(stack_pomc)/G_hat0+S2_hat0)
stack_pomc_stab_rss = np.sum((stack_pomc_stab-
                              stack_pomc_stab.mean(2).reshape((60,80,1)))**2,2)

from scipy.stats import chi2
plt.figure(dpi=600,figsize=(10,8))
plt.subplot(121)
plt.contour(range(80),range(60),
            np.log(chi2.sf(np.flip(stack_pomc_stab_rss,axis=0),
                                        df=stack_pomc_stab.shape[2]-1)),
            linewidths=2,linestyles='solid',colors='black')
plt.grid()
plt.xlim([23,61])
plt.xticks(color='w')
plt.yticks(color='w')
plt.title('Wide field', fontsize=20)
plt.subplot(122)
plt.contour(range(80),range(60),
            np.log(chi2.sf(np.flip(stack_pomc_stab_rss,axis=0),
                           df=stack_pomc_stab.shape[2]-1)),
            linewidths=2,linestyles='solid',colors='black')
plt.ylim([25,38])
plt.xlim([33,43])
plt.xticks(color='w')
plt.yticks(color='w')
plt.title('Cell body', fontsize=20)
plt.grid()
fname = '../figs/stack-contour.png'
plt.savefig(fname)
plt.close()
fname

threshold = -300
good_pix = np.where(np.log(chi2.sf(stack_pomc_stab_rss,
                                   df=stack_pomc_stab.shape[2]-1)) < -300)
data4fit = stack_pomc_stab[good_pix[0],good_pix[1],:]

def mk_residual_pomc(data=data4fit,
                     S2=S2_hat0,
                     base_length=10):
    npix,K = data.shape
    f = np.ones((K,))
    pred = np.zeros(data.shape)
    def residual(par):
        b = par[0]
        phi = par[1:(npix+1)]
        f[:]=1.
        f[base_length:] = par[(npix+1):]
        pred[:,:] = 2*np.sqrt(np.outer(phi,f)+b+S2)
        return (data-pred).flatten()
    return residual

baseline = stack_pomc[good_pix[0],good_pix[1],:10].mean(1)
f_0 = np.mean(stack_pomc[good_pix[0],good_pix[1],:]/baseline.reshape((12,1)),0)
b_0 = 100
phi_0 = np.mean((stack_pomc[good_pix[0],good_pix[1],:]-b_0)/f_0.reshape((1,168)),1)
#par_0 = np.zeros((1+12+163,))
par_0 = np.zeros((1+12+158,))
par_0[0] = b_0
par_0[1:13] = phi_0
par_0[13:] = f_0[10:]

residual_pomc = mk_residual_pomc()
res = leastsq(residual_pomc,par_0,full_output=True)
print(res[3])

def mk_residual_pomc_exp(data=data4fit,
                         S2=S2_hat0,
                         base_length=15,
                         delta = 0.15):
    npix,K = data.shape
    f = np.ones((K,))
    pred = np.zeros(data.shape)
    tt = np.array([(i-base_length)*delta for i in range(K)])
    def residual(par):
        b = par[0]
        phi = par[1:(npix+1)]
        f[:]=1.
        Delta = par[(npix+1)]
        beta = par[-1]
        f[base_length:] +=  Delta * np.exp(-beta * tt[base_length:]) 
        pred[:,:] = 2*np.sqrt(np.outer(phi,f)+b+S2)
        return (data[:,15:]-pred[:,15:]).flatten()
    return residual

par_A = np.zeros((1+12+2,))
par_A[0] = b_0
par_A[1:13] = phi_0
par_A[13] = f_0[15]
par_A[14] = 0.1

residual_pomc_exp = mk_residual_pomc_exp()
res_exp = leastsq(residual_pomc_exp,par_A,full_output=True)
print(res[3])

f_hat = np.ones((168,))
f_hat[10:] = res[0][13:]
par_se = np.sqrt(np.diag(res[1]))
f_up = np.ones((168,))
f_up[10:] = res[0][13:] + 1.96*par_se[13:]
f_low = np.ones((168,))
f_low[10:] = res[0][13:] - 1.96*par_se[13:]
fig, ax = plt.subplots(dpi=300,figsize=(10,8))
ax.fill_between(np.arange(168)*0.15,f_low,f_up,facecolor='grey',edgecolor='grey')
ax.plot(np.arange(168)*0.15,f_hat,lw=3,color='black')
plt.grid()
plt.xticks(fontsize=20)
plt.yticks(fontsize=20)
plt.xlabel('Time (s)',fontsize=25)
plt.ylabel(r'$\hat{f}_k$',fontsize=25)
plt.xlim([0,25])
fname = "../figs/time-course-fit_chap.png"
plt.savefig(fname)
plt.close()
fname

def mk_pred(p_vector,
            stabilise_var=True,
            S2=S2_hat0,
            G=G_hat0,
            base_length=10):
    phi = p_vector[1:13]
    b = p_vector[0]
    f = np.ones((168,))
    f[base_length:] = p_vector[13:]
    pred = np.outer(phi,f)+b
    if stabilise_var:
        return 2*np.sqrt(pred+S2)
    else:
        return G*pred

pred = mk_pred(res[0],True)

np.sum((data4fit-pred)**2)

np.sum((data4fit-mk_pred(par_0,True))**2)

from scipy.stats import chi2
chi2.sf(np.sum((data4fit-pred)**2),12*168-len(res[0]))

def mk_pred_exp(p_vector,
                stabilise_var=True,
                S2=S2_hat0,
                G=G_hat0,
                base_length=15,
                delta = 0.15):
    tt = np.array([(i-base_length)*delta for i in range(168)])
    phi = p_vector[1:13]
    b = p_vector[0]
    Delta = p_vector[13]
    beta = p_vector[14]
    f = np.ones((168,))
    f[base_length:] +=  Delta * np.exp(-beta * tt[base_length:]) 
    pred = np.outer(phi,f)+b
    if stabilise_var:
        return 2*np.sqrt(pred+S2)
    else:
        return G*pred

pred_exp = mk_pred_exp(res_exp[0],True)

np.sum((data4fit[:,15:]-pred_exp[:,15:])**2)

np.sum((data4fit[:,15:]-mk_pred_exp(par_A,True)[:,15:])**2)

from scipy.stats import chi2
chi2.sf(np.sum((data4fit[:,15:]-pred_exp[:,15:])**2),12*(168-15)-len(res_exp[0]))

the_range = [np.min(data4fit),np.max(data4fit)]
xx = np.arange(168)*0.15
fig = plt.figure(dpi=300,figsize=(10,8))
gs = fig.add_gridspec(3, 4, hspace=0, wspace=0)
axs = gs.subplots(sharex=True, sharey=True)
total = 0
for i in range(3):
    for j in range(4):
        axs[i,j].plot(xx[15:],pred_exp[total,15:],color='red')
        axs[i,j].plot(xx,data4fit[total,:],color='grey',lw=0.75)
        total += 1
for ax in fig.get_axes():
    ax.label_outer()
plt.xlim([0,25])
plt.ylim(the_range)
fname = "../figs/data-and-fit_chap.png"
plt.savefig(fname)
plt.close()
fname
